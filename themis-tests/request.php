<?php

use Charm\Http\Message\Request;
use Charm\Http\Message\ServerRequest;
use Charm\Http\Message\Stream;
use Charm\Http\Message\UploadedFile;

require(__DIR__ . '/../vendor/autoload.php');

$request = new Request("GET", "http://www.vg.no/");
$serverRequest = new ServerRequest("GET", "http://www.vg.no/", new Stream(fopen(__FILE__, 'r')), ['Connection' => ['Close']]);
$uf = new UploadedFile(__FILE__);
