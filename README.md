charm/http
==========

A very pure, extendable set of traits and classes that implement PSR-7 HTTP
message interfaces from https://www.php-fig.org/psr/psr-7/.

The library places no restrictions on extending the classes or overriding
any method implementations. In fact, the main reason why it exists is that
most other implementations specifically prohibits extending or overriding 
their implementation.


Classes
-------

Each of these classes are convenience classes which use the traits. You may use
them directly, or you may implement your own variants by using the traits.

 * `Charm\Http\Message\Message`
 * `Charm\Http\Message\Request`
 * `Charm\Http\Message\ServerRequest`
 * `Charm\Http\Message\Response`
 * `Charm\Http\Message\Uri`
 * `Charm\Http\Message\Stream`
 * `Charm\Http\Message\UploadedFile`


Traits
------

 * `Charm\Http\Message\MessageTrait` implements `Psr\Http\Message\MessageInterface`
 * `Charm\Http\Message\RequestTrait` implements `Psr\Http\Message\RequestInterface`
 * `Charm\Http\Message\ServerRequestTrait` implements `Psr\Http\Message\ServerRequestInterface`
 * `Charm\Http\Message\ResponseTrait` implements `Psr\Http\Message\ResponseInterface`
 * `Charm\Http\Message\UriTrait` implements `Psr\Http\Message\UriInterface`
 * `Charm\Http\Message\StreamTrait` implements `Psr\Http\Message\StreamInterface`
 * `Charm\Http\Message\UploadedFileTrait` implements `Psr\Http\Message\UploadedFileInterface`
