<?php
namespace Charm\Http\Message;

use JsonSerializable;
use Psr\Http\Message\StreamInterface;
use function json_encode, is_scalar;

/**
 * Describes a data stream.
 *
 * Typically, an instance will wrap a PHP stream; this interface provides
 * a wrapper around the most common operations, including serialization of
 * the entire stream to a string.
 */
class Stream implements StreamInterface {
    use StreamTrait;

    private static function isJsonSerializable(mixed $value): bool {
        if (is_scalar($value) || $value instanceof JsonSerializable) {
            return true;
        } elseif (is_array($value)) {
            foreach ($value as $k => $v) {
                if (!static::isJsonSerializable($v)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Take a value and ensure it is a Stream instance. If the provided
     * value is already an instance of StreamInterface, the provided value
     * is returned unmodified.
     *
     * If a content type hint is provided, an attempt will be made to correctly
     * encode the message body according to that content type. Currently supported
     * content types are:
     *
     *  * "application/json"
     *    Serializes the value to JSON.
     *
     *  * "application/x-www-form-urlencoded"
     *    Serializes the value according to RFC1738 for use in typical HTTP POST
     *    requests.
     *
     */
    public static function cast(mixed $source, string $contentType=null): static {
        if ($source instanceof StreamInterface) {
            return $source;
        }

        if ($contentType !== null) {
            switch ($contentType) {
                case 'application/json':
                    if (static::isJsonSerializable($source)) {
                        $source = \json_encode($source, \JSON_THROW_ON_ERROR);
                    } else {
                        throw new \RuntimeException("Source value can't be serialized into 'application/json' format");
                    }
                    break;
                case 'application/x-www-form-urlencoded':
                    if (!is_array($source) && !is_object($source)) {
                        throw new \RuntimeException("Source value must be an array or an object for serializing into 'application/x-www-form-urlencoded' format");
                    }
                    $source = \http_build_query($source, '', null, \PHP_QUERY_RFC1738);
                    break;
                default:
                    throw new \LogicException("Content type '$contentType' is not valid for cast() currently");
            }
            $fp = \fopen('php://temp', 'w+');
            \fwrite($fp, (string) $source);
            \rewind($fp);
            return new static($fp);
        } elseif (\is_scalar($source) || $source instanceof \Stringable) {
            $fp = \fopen('php://temp', 'w+');
            \fwrite($fp, (string) $source);
            \rewind($fp);
            return new static($fp);
        } elseif (\is_resource($source)) {
            switch (\get_resource_type($source)) {
                case 'stream':
                    return new static($source);
                default:
                    throw new \InvalidArgumentException("Unsupported stream resource type");
            }
        } else {
            throw new \InvalidArgumentException(\get_debug_type($source)." can't be cast to ".static::class);
        }
    }

    /**
     * @param resource $stream The stream resource to represent
     */
    public function __construct($stream) {
        $this->StreamTrait($stream);
    }

    /**
     * @deprecated Use cast() instead
     */
    public static function create(mixed $source): static {
        return static::cast($source);
    }
}
