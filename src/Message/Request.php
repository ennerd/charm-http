<?php
namespace Charm\Http\Message;

use Psr\Http\Message\{
    RequestInterface,
    UriInterface
};

/**
 * Representation of an outgoing, client-side request.
 *
 * Per the HTTP specification, this interface includes properties for
 * each of the following:
 *
 * - Protocol version
 * - HTTP method
 * - URI
 * - Headers
 * - Message body
 *
 * During construction, implementations MUST attempt to set the Host header from
 * a provided URI if no Host header is provided.
 *
 * Requests are considered immutable; all methods that might change state MUST
 * be implemented such that they retain the internal state of the current
 * message and return an instance that contains the changed state.
 */
class Request implements RequestInterface {
    use RequestTrait;

    /**
     * Configure the request.
     *
     * @param string $method                        Case-sensitive method.
     * @param string|UriInterface $uri              Request URI
     * @param string|resource|StreamInterface $body Body
     * @param string[][] $headers                   Array of header names => values
     * @param string $protocolVersion               The HTTP protocol version, typically "1.1" or "1.0"
     */
    public function __construct(string $method, mixed $uri, mixed $body='', array $headers=[], string $protocolVersion='1.1') {
        $this->RequestTrait($method, $uri, $body, $headers, $protocolVersion);
    }
}
